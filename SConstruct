# Static Website Builder
# A simple and lightweight static website building script based on SCons.
# Author: Donghao Ren

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
import os
import hashlib
import subprocess
from datetime import datetime

# Minify JS and CSS or not.
option_minify = ARGUMENTS.get('minify', 'true')

# Current time.
build_time = datetime.now()

# Meta strings, can be used in files with syntax {{key}}
# Only 0-9a-zA-Z and -, _, . are allowed for key.
meta_strings = {
    'build_time'     : build_time.strftime("%B %d, %Y, %I:%M %p"),
    'build_time_iso' : build_time.isoformat()
}

# Global variables.
output_directory = "deploy"
temporary_directory = "temp"

# Delimiters
delim_L = "{{"
delim_R = "}}"

def Delimiter(L, R):
    global delim_L
    global delim_R
    delim_L = L
    delim_R = R
    reg_L = re.escape(delim_L)
    reg_R = re.escape(delim_R)
    global regex_partial, regex_include, regex_js, regex_css, regex_ref, regex_meta;
    regex_partial = re.compile(reg_L + r' *partial: *([0-9a-zA-Z\-\_\.]+) *' + reg_R)
    regex_include = re.compile(reg_L + r' *include: *([0-9a-zA-Z\-\_\.]+) *' + reg_R)
    regex_js = re.compile(reg_L + r" *js: *([0-9a-zA-Z\-\_\.\,\/]+) *" + reg_R)
    regex_css = re.compile(reg_L + r" *css: *([0-9a-zA-Z\-\_\.\,\/]+) *" + reg_R)
    regex_ref = re.compile(reg_L + r" *ref: *([0-9a-zA-Z\-\_\.\,\/]+) *" + reg_R)
    regex_meta = re.compile(reg_L + r" *([0-9a-zA-Z\_\-\.]+) *" + reg_R)

Delimiter("{{", "}}")

# Resolve include files.
# soruce: scons file object.
def resolve_includes(source):
    global regex_include
    data = source.get_text_contents()
    data = regex_include.sub(lambda m: resolve_includes(File(m.group(1))), data)
    return data

def include_build_function(target, source, env, minify = ''):
    data = "";
    for s in source:
        data += resolve_includes(s)

    for t in target:
        fn = str(t)
        if minify == 'js' or minify == 'css':
            p = subprocess.Popen(["java", "-jar", "utils/yuicompressor.jar", "--type", minify, "-o", fn], stdin = subprocess.PIPE, stdout = None, stderr = subprocess.STDOUT)
            p.stdin.write(data)
            p.stdin.close()
            r = p.wait()
            if r != 0:
                return Exception("Failed to build %s" % fn)
        else:
            f = open(str(fn), 'w')
            f.write(data)
            f.close()
    return None

def inc_build_function(target, source, env):
    return include_build_function(target, source, env)

def js_build_function(target, source, env):
    return include_build_function(target, source, env, minify = 'js')

def css_build_function(target, source, env):
    return include_build_function(target, source, env, minify = 'css')

# JS and CSS builder.
if option_minify == 'true':
    js_builder = Builder(action = js_build_function)
    css_builder = Builder(action = css_build_function)
else:
    js_builder = Builder(action = inc_build_function)
    css_builder = Builder(action = inc_build_function)

# Markdown builder.
markdown_builder = Builder(action = 'cat $SOURCES | markdown > $TARGET')

# Copy builder, just copy files.
copy_builder = Builder(action = 'cp $SOURCE $TARGET')

# Concat builder, concat source files to target files, a line-break is
# inserted between files.
def concat_build_function(target, source, env):
    data = "";
    for s in source:
        data += s.get_text_contents() + "\n"
    
    for t in target:
        f = open(str(t), 'w')
        f.write(data)
        f.close()
    return None

concat_builder = Builder(action = concat_build_function)

# Template scanner, scan for partials, make them dependencies.
def template_scanner(node, env, path):
    files = [];
    text = node.get_text_contents()
    result = regex_partial.findall(text)
    for partial in result:
        files.append(get_partial_path(partial))
    return env.File(files);

# JS/CSS scanner, scan for included files.
def jscss_scanner(node, env, path, parents = []):
    files = [];
    text = node.get_text_contents()
    path = os.path.dirname(node.rstr())
    if path == "":
        path = "."
    result = regex_include.findall(text)
    for inc in result:
        if inc in parents:
            raise Exception("Circular includes on '%s'." % str(node))
        files.append(path + "/" + inc)
    r = env.File(files);
    for inc in result:
        r += jscss_scanner(File(inc), env, path, parents + [inc])
    return r

# Substitute builder, Template + Partials + HTML = Output Page.
def substitute_build_function(target, source, env):
    template = source[1].get_text_contents();
    content = source[0].get_text_contents();
    template = template.replace(delim_L + "content" + delim_R, content);

    template = regex_partial.sub(lambda m: read_partial_file(m.group(1)), template)
    for t in target:
        f = open(str(t), 'w')
        f.write(template)
        f.close()
    return None
    
substitute_builder = Builder(action = substitute_build_function)



# HTML builder function.
# Change {{js: js_files}}, {{css: css_files}} to compiled locations.
def html_build_function(target, source, env):
    global regex_js, regex_css, regex_ref, regex_meta
    
    data = "";
    for s in source:
        data += s.get_text_contents()

    data = regex_meta.sub(lambda m: meta_substitute(m.group(1), m.group(0), meta_strings), data)
    
    local_meta = {
        'title': env['SWB_title'],
        'pageurl': env['SWB_url']
    }
    
    data = regex_meta.sub(lambda m: meta_substitute(m.group(1), m.group(0), local_meta), data)

    for t in target:
        fn = str(t);

        html = regex_js.sub(lambda m: make_relative_path(fn, m.group(1)), data)
        html = regex_css.sub(lambda m: make_relative_path(fn, m.group(1)), html)
        html = regex_ref.sub(lambda m: make_relative_path(fn, m.group(1)), html)
        
        html = html.replace(delim_L + "L" + delim_R, delim_L)
        html = html.replace(delim_L + "R" + delim_R, delim_R)

        f = open(fn, 'w')
        f.write(html)
        f.close()
    return None

html_builder = Builder(action = html_build_function)

# ImageMagick builder.
def imagemagick_generator(source, target, env, for_signature):
    return 'convert "%s" %s "%s"' % (source[0], env['SWB_args'], target[0])

imagemagick_builder = Builder(generator = imagemagick_generator)

# The SCons environment.
env = Environment(
    BUILDERS = {
        'Javascript' : js_builder,
        'CSS' : css_builder,
        'Concat' : concat_builder,
        'Copy' : copy_builder,
        'Markdown' : markdown_builder,
        'Substitute' : substitute_builder,
        'HTML' : html_builder,
        'ImageMagick' : imagemagick_builder
    }
);

# Add our template scanner.
env.Append(SCANNERS = Scanner(function = template_scanner, skeys = ['.template']))
env.Append(SCANNERS = Scanner(function = jscss_scanner, skeys = ['.js', '.css']))

# Utility functions.
def get_file_extension(filename):
    return os.path.splitext(filename)[1][1:].strip().lower();

def get_partial_path(name):
    return "%s/%s.partial" % (temporary_directory, name)

def read_partial_file(name):
    temp = get_partial_path(name)
    f = open(temp, 'r')
    content = f.read()
    f.close()
    return content

def get_temp_path(url):
    return "%s/%s.content" % (temporary_directory, url)
    
def content_html(target, source):
    extension = get_file_extension(source)
    if extension == "html":
        env.Copy(target, source)
    elif extension == "md":
        env.Markdown(target, source)

def make_relative_path(html_path, file_path):
    html_path = html_path.replace(output_directory + "/", "");
    return os.path.relpath(file_path, os.path.dirname(html_path));

def meta_substitute(meta_name, original, metadict):
    if meta_name in metadict:
        return metadict[meta_name]
    return original

# Set output directory.
def OutputDirectory(dir):
    global output_directory
    output_directory = dir

# Set temporary directory.
def TemporaryDirectory(dir):
    global temporary_directory
    temporary_directory = dir

# Add partial.
def Partial(name, source):
    target_name = get_partial_path(name)
    content_html(target_name, source)

# Add page.
def Page(url, source, template, title = ''):
    temp_name = get_temp_path(url)
    content_html(temp_name, source)
    temp = "%s/%s.compiled" % (temporary_directory, url)
    output = "%s/%s" % (output_directory, url)
    env.Substitute(temp, [ temp_name, template ])
    env.HTML(output, temp, SWB_title = title, SWB_url = url)
    
# Add pure HTML file, just expand metadata.
def HTML(url, source, title = ''):
    output = "%s/%s" % (output_directory, url)
    extension = get_file_extension(source)
    if extension == 'html':
        env.HTML(output, source, SWB_title = title, SWB_url = url)
    else:
        temp = "%s/%s.compiled" % (temporary_directory, url)
        env.Markdown(temp, source)
        env.HTML(output, temp, SWB_title = title, SWB_url = url)        

# Add Javascript, source can be multiple files.
def Javascript(url, source):
    mins = [];
    output = "%s/%s" % (output_directory, url)
    for s in source:
        min = "%s/%s.min" % (temporary_directory, s)
        env.Javascript(min, s)
        mins.append(min)
    env.Concat(output, mins)

# Add CSS, source can be multiple files.
def CSS(url, source):
    mins = [];
    output = "%s/%s" % (output_directory, url)
    for s in source:
        min = "%s/%s.min" % (temporary_directory, s)
        env.CSS(min, s)
        mins.append(min)
    env.Concat(output, mins)

def Binary(url, source):
    output = "%s/%s" % (output_directory, url)
    env.Copy(output, source)

def Binaries(url_path, list):
    for file in list:
        Binary(url_path + file, file)

def Image(url, source):
    Binary(url, source)

def ImageMagick(url, source, args = ""):
    output = "%s/%s" % (output_directory, url)
    env.ImageMagick(output, source, SWB_args = args)
    
def Images(url_path, list):
    Binaries(url_path, list)
    
def Find(pattern, directory = "."):
    dir = Dir(directory)
    r = dir.glob(pattern, strings=True)
    return r

# Meta strings.
def Meta(key, value):
    global meta_strings;
    meta_strings[key] = value

# Load Website metadata.
execfile("WebsiteMeta")
