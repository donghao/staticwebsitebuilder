Static Website Builder
====

A simple and lightweight static website building script based on SCons.

* Author: Donghao Ren

* License: GPLv3

Concepts
----

* Templates: HTML templates are style files for your website. They are
  incomplete HTML files with content left blank.
  
    - Use .template extension for templates (required).
    - Use {{content}} to indicate where to place content.
    - Use {{partial: partial name}} to place partials.
  
* Partials: Contents that are common to many pages, for example sidebar,
  menus.

* Meta Strings:

    - build\_time: time the build script run.
    - build\_time\_iso: iso format of build_time.
    - User defined meta strings.

Guide
----

First, decide the website structure, and create your WebsiteMeta file.
The WebsiteMeta file is a python script. Use the following functions to
define your website:

* Specify the output directory.

    - OutputDirectory("deploy")

* Specify the directory for temporary files.

    - TemporaryDirectory("temp")

* Partials.

    - Partial("sidebar", "sidebar.md")

* Pages.

    - Page("index.html", "index.md", "template.template", title = "title")

* Binary files.

    - Binary("output.bin", "input.bin")
    - Binaries("output_dir", File("pattern"))

* Meta Strings.

    - Meta("key", "string")
  
* Javascript and CSS files.

    - Javascript("output.js", [ "input1.js", "input2.js" ])
    - CSS("output.css", [ "input1.css", "input2.css" ])

Second, write your templates. A template is a incomplete HTML file, with
a content placeholder and references to partials.

* {{content}}: put content here.

* {{partial: partial_name}}: put partial here.

    - Partial names can have only digits, english characters, -, _, dot.

* {{meta_name}}: put meta string here.

    - build\_time: time of the build.
    - build\_time\_iso: build\_time in iso date format.

* Including css or script files

    - Use {{css: cssfile1.css, cssfile2.css}} to include css files.
    - Use {{js: jsfile1.css, jsfile2.css}} to include script files.

* You may want to use another delimiter instead of {{ and }} for your website:

    - Delimiter("left", "right") to set another pair.
    - Use {{L}} and {{R}} for displaying delimiter themselves.
    
Then, add your content. You can use Markdown or HTML files as input.

Finally, run scons to build the website!